FROM openjdk:8-jdk-alpine

COPY target/cicd.jar /cicd/

EXPOSE 8095

WORKDIR /cicd

ENTRYPOINT java -jar cicd.jar
